#include <stdlib.h>
#include <math.h>


double ra01()
{
  //   return(double(rand())/RAND_MAX);
  return((double)(rand())/RAND_MAX);
}
    
double noise()
{
  static int iset=0;
  static double gset;
   
  double fac,rsq,v1,v2;
   
  if(iset==0) {
    do {
      v1=2.0*ra01()-1.;
      v2=2.0*ra01()-1.;
      rsq=v1*v1+v2*v2;
    } while(rsq>1.0 || rsq==0);
   
    fac=sqrt(-2.0*log(rsq)/rsq);
    gset=v1*fac;
    iset=1;
    return v2*fac;
  } else {
    iset=0;
    return gset;
  }
}

