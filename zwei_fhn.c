#include "noise.h"
#include <math.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define tau 10000

struct Pars {
  double a;
  double epsilon;
  double D;
  double dt;
};

struct X {
  double u;
  double v;
};

short asPCM(double in, double max) {
  return (in / max) * SHRT_MAX;
}

struct X* dynamics(struct X* x, const struct Pars* par, struct X* xAlt, struct X* yAlt) {
  struct X next;
  next.u = (x->u - (x->u)*(x->u)*(x->u)/3. - x->v + (yAlt->u - x->u)) / par->epsilon;
  next.v = x->u + par->a;
  x->u += next.u * par->dt;
  x->v += next.v * par->dt + par->D*noise()*sqrt(par->dt);
  return x;
}

int main(int argc, char **argv) {
  srand((unsigned)time(0));

  struct Pars par1 = {.a = 1.05, .epsilon = 1.e-3, .D = 0., .dt = 1.e-4};
  struct Pars par2 = {.a = 1.05, .epsilon = 1.e-3, .D = 0., .dt = 1.e-4};
  struct X x[tau];
  struct X y[tau];
  x[0].u = 0.1; x[0].v = 0.;
  y[0].u = 0.; y[0].v = 0.1;
  for (int i=1; i<tau; ++i) {
    x[i].u = 1.;
    x[i].v = 1.;
    y[i].u = 1.;
    y[i].v = 1.;
  }
  

  int pointer = 0;
  for (int t=0; t<4e8; ++t) {
    int now = pointer % tau;
    int next = (pointer+1) % tau;
    int delayed = (pointer+2) % tau;
    x[next] = *dynamics(&x[now], &par1, &x[delayed], &y[delayed]);
    y[next] = *dynamics(&y[now], &par2, &y[delayed], &x[delayed]);
    if (!(t % 322)) {
      //printf("%i %f %f %i\n", t, x.u, x.v, val);
      short valX = asPCM(x[next].u, 2.1);
      fwrite(&(valX), sizeof(valX), 1, stdout);
      short valY = asPCM(y[next].u, 2.1);
      fwrite(&(valY), sizeof(valY), 1, stdout);
    }
    ++pointer;
  }

  return 0;
}
