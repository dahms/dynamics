#include "noise.h"
#include <math.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

struct Pars {
  double b;
  double D;
  double dt;
};

struct X {
  double x;
  double y;
};

short asPCM(double in, double max) {
  return (in / max) * SHRT_MAX;
}

struct X* dynamics(struct X* u, const struct Pars* par) {
  struct X next;
  const double bracket = 1. - (u->x)*(u->x) - (u->y)*(u->y); 
  next.x = u->x * bracket + u->y * (u->x - par->b);
  next.y = u->y * bracket - u->x * (u->x - par->b);
  u->x += next.x * par->dt + par->D*noise()*sqrt(par->dt);
  u->y += next.y * par->dt + par->D*noise()*sqrt(par->dt);
  return u;
}

int main(int argc, char **argv) {
  srand((unsigned)time(0));

  struct Pars par = {.b = 0.95, .D = 0.15, .dt = 1.e-3};
  struct X x = {-1., 0.};

  for (int t=0; t<4e8; ++t) {
    x = *dynamics(&x, &par);
    /*if (!(t % 40000000)) {
      par.D += 0.01;
      //fprintf(stderr, "%g\n", par.D);      
      }*/
    if (!(t % 2000000)) {
      fprintf(stderr, "replot\n");
      fprintf(stderr, "clear\n");
    }
    if (!(t % 196*100)) {
      fprintf(stderr, "%f %f\n", x.x, x.y);
    }
    if (!(t % 196)) {
      //fprintf(stderr, "%i %f %f\n", t, x.x, x.y); // -> debug for frequency
      short val = asPCM(x.x, 1.5);
      fwrite(&(val), sizeof(val), 1, stdout);
    }
  }

  return 0;
}
