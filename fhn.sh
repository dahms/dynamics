#!/bin/bash

function ensure_fifo {
if [[ ! -p $1 ]]; then
    mkfifo "$1"
    return $?
fi
}

ensure_fifo audio || exit 1
ensure_fifo plotdata || exit 1

play -t raw -r 44k -e signed -b 16 -c 1 - < audio &

~/feedgnuplot/bin/feedgnuplot --stream trigger --domain --xmin -2.5 --xmax 2.5 --ymin -1 --ymax 1 < plotdata &

./fhn >audio 2>/dev/null
