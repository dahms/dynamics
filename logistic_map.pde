float r = 2.4;
float x = 0.5;
int windowsize = 600;
int zoom = 7;
float[] x_t = new float[windowsize/zoom];
int windowpointer = 0;

void setup() {
  frameRate(60/zoom);
  size(600, 600); 
  background(255);
}

void draw() {
  r = lerp(r, 2.4+float(mouseX)/windowsize*1.6, 1.);

  String txt = "r=" + r;
  fill(255);
  noStroke();
  rect(10, windowsize-40, 400, 25);
  textSize(18);
  stroke(0);
  fill(0);
  text(txt, 12, windowsize-22);
  fill(255);

  x = r*x*(1.-x);
  x_t[windowpointer] = x;
  
  for (int i=1; i<windowpointer;++i) {
    int x1 = zoom*(i-1);
    int x2 = zoom*i;
    int u1 = int(windowsize*(1-x_t[i-1]));
    int u2 = int(windowsize*(1-x_t[i]));
    stroke(50,100,100);
    line(x1, u1, x2, u2);
  }
  ++windowpointer;
  if (windowpointer >= windowsize/zoom) {
    windowpointer = 0;
    background(255);
  }
}

void mousePressed() {
  if (mouseButton == LEFT) {
    background(255);
    windowpointer = 0;
  }   
}