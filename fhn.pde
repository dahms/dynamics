float dt = 1.e-4;
float a = 1.3;
float u;
float v;
float epsilon = 1.e-3;
int direction;
int windowsize = 600;
float[] u_t = new float[windowsize];
float[] v_t = new float[windowsize];
int windowpointer = 0;
float D = 0;
java.util.Random r = new java.util.Random();

void setup() {
  size(600, 600); 
  background(255);
}

void draw() {
  a = lerp(a, 0.9+float(mouseX)/windowsize*0.5, 0.05);
  D = lerp(D, float(windowsize-mouseY)/windowsize*0.01, 0.05);

  String txt = "a=" + a + ", D=" + D;
  fill(255);
  noStroke();
  rect(10, windowsize-40, 400, 25);
  textSize(18);
  stroke(0);
  fill(0);
  text(txt, 12, windowsize-22);
  fill(255);

  double noise = r.nextGaussian();
  int iperd = 2000;
  float[] U = new float[iperd];
  float[] V = new float[iperd];
  for (int i=0;i<iperd;++i) {
    u += dt*(u-u*u*u/3.-v)/epsilon;
    v += dt*(u+a) + D*noise*sqrt(dt);
    U[i] = u;
    V[i] = v;
  }
  u_t[windowpointer] = u;
  v_t[windowpointer] = v;
  
  for (int i=1; i<windowpointer;++i) {
    int x1 = i-1;
    int x2 = i;
    int u1 = int(windowsize/4*(1-u_t[i-1]/2.1));
    int u2 = int(windowsize/4*(1-u_t[i]/2.1));
    stroke(200,50,50);
    line(x1, u1, x2, u2);
    int v1 = int(windowsize/4*(1-v_t[i-1]/2.1));
    int v2 = int(windowsize/4*(1-v_t[i]/2.1));
    stroke(50,200,50);
    line(x1, v1, x2, v2);
  }
  ++windowpointer;
  if (windowpointer == windowsize) {
    windowpointer = 0;
    background(255);
  }

  for (int i=0;i<iperd;++i) {
    stroke(190);
    fill(255);
    ellipse(windowsize/2+windowsize/4.2*U[i], windowsize*3/4-windowsize/4.2*V[i], 7, 7);
  }
}

void mousePressed() {
  if (mouseButton == LEFT) {
    background(255);
    windowpointer = 0;
  }   
}