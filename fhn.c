#include "noise.h"
#include <math.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

struct Pars {
  double a;
  double epsilon;
  double D;
  double dt;
};

struct X {
  double u;
  double v;
};

short asPCM(double in, double max) {
  return (in / max) * SHRT_MAX;
}

struct X* dynamics(struct X* x, const struct Pars* par) {
  struct X next;
  next.u = (x->u - (x->u)*(x->u)*(x->u)/3. - x->v) / par->epsilon;
  next.v = x->u + par->a;
  x->u += next.u * par->dt;
  x->v += next.v * par->dt + par->D*noise()*sqrt(par->dt);
  return x;
}

int main(int argc, char **argv) {
  srand((unsigned)time(0));

  struct Pars par = {.a = 1.05, .epsilon = 1.e-3, .D = 0., .dt = 1.e-4};
  struct X x = {0., 0.};

  for (int t=0; t<4e8; ++t) {
    x = *dynamics(&x, &par);
    if (!(t % 40000000)) {
      par.D += 0.01;
      //fprintf(stderr, "%g\n", par.D);      
    }
    if (!(t % 2000000)) {
      fprintf(stderr, "replot\n");
      fprintf(stderr, "clear\n");
    }
    if (!(t % 32250)) {
      fprintf(stderr, "%f %f\n", x.u, x.v);
    }
    if (!(t % 322)) {
      short val = asPCM(x.u, 2.1);
      fwrite(&(val), sizeof(val), 1, stdout);
    }
  }

  return 0;
}
