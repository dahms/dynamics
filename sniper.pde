fhfloat dt = 1.e-3;
float b = 1.05;
float x;
float y;
int windowsize = 600;
float[] x_t = new float[windowsize];
float[] y_t = new float[windowsize];
int windowpointer = 0;
float D = 0;
java.util.Random r = new java.util.Random();
int tau = int(9./dt);
float[] x_d = new float[tau];
float[] y_d = new float[tau];
int dpointer = 0;
boolean delay_on = false;
boolean noise_on = false;

void setup() {
  size(600, 600); 
  background(255);
}

void draw() {
  b = lerp(b, 0.5+float(mouseX)/windowsize*1.0, 1.);
  D = lerp(D, float(windowsize-mouseY)/windowsize*0.5, 1.);

  float K = delay_on ? 0.4 : 0.;
  String txt = "b=" + b + ", D=" + (noise_on ? D : 0.) + ", K=" + K;
  fill(255);
  noStroke();
  rect(10, windowsize-40, 400, 25);
  textSize(18);
  stroke(0);
  fill(0);
  text(txt, 12, windowsize-22);
  fill(255);

  int iperd = 500;
  float[] X = new float[iperd];
  float[] Y = new float[iperd];
  for (int i=0;i<iperd;++i) {
    double noise1 = noise_on ? r.nextGaussian() : 0.;
    double noise2 = noise_on ? r.nextGaussian() : 0.;
    float bracket = 1.-x*x-y*y;
    int at_tau = (dpointer+1) % tau;
    x += dt*(x*bracket+y*(x-b) + K*(x_d[at_tau] - x)) + D*noise1*sqrt(dt);
    y += dt*(y*bracket-x*(x-b) + K*(y_d[at_tau] - y)) + D*noise2*sqrt(dt);
    x_d[at_tau] = x;
    y_d[at_tau] = y;
    X[i] = x;
    Y[i] = y;
    ++dpointer;
  }
  x_t[windowpointer] = x;
  y_t[windowpointer] = y;
  
  for (int i=1; i<windowpointer;++i) {
    int x1 = i-1;
    int x2 = i;
    int x_1 = int(windowsize/4*(1-x_t[i-1]/1.5));
    int x_2 = int(windowsize/4*(1-x_t[i]/1.5));
    stroke(200,50,50);
    line(x1, x_1, x2, x_2);
    int y_1 = int(windowsize/4*(1-y_t[i-1]/1.5));
    int y_2 = int(windowsize/4*(1-y_t[i]/1.5));
    stroke(50,200,50);
    line(x1, y_1, x2, y_2);
  }
  ++windowpointer;
  if (windowpointer == windowsize) {
    windowpointer = 0;
    background(255);
  }

  stroke(50);
  fill(255);
  for (int i=0;i<iperd;++i) {
    ellipse(windowsize/2+windowsize/5*X[i], windowsize*3/4-windowsize/5*Y[i], 7, 7);
  }
  
  stroke(100);
  noFill();
  for (float p=0;p<2*PI;p+=2*PI/50) {  
    arc(windowsize/2, windowsize*3/4, windowsize/2.5, windowsize/2.5, p, p+2*PI/100);
  }
}

void mousePressed() {
  if (mouseButton == LEFT) {
    background(255);
    windowpointer = 0;
  }
  else if (mouseButton == RIGHT) {
    noise_on = !noise_on;
  }
}

void mouseWheel(MouseEvent event) {
  delay_on = event.getCount() > 0 ? true : false;
}